<?php
//incluir archivo de conexion
	include("conexion.php");

//crear la consulta para listar datos
	$consulta = "SELECT
					id_prod,
					nombre,
					marca,
					precio,
					observaciones
				FROM prods";

	$ejecuta = $conexion -> query($consulta) or die("Error al listar productos <br> :" . $conexion -> error);

?>

<table id="lista_productos">
	<tr>
		<th>ID</th>
		<th>Nombre</th>
		<th>Marca</th>
		<th>Precio</th>
		<th>Obervaciones</th>
    <th colspan="2">Edicion</th>
	</tr>
<?php

while( $arreglo_resultados = $ejecuta -> fetch_row() ){
		echo '<tr>';
		echo '<td>' . $arreglo_resultados[0] . '</td>';
		echo '<td>' . $arreglo_resultados[1] . '</td>';
		echo '<td>' . $arreglo_resultados[2] . '</td>';
		echo '<td>' . $arreglo_resultados[3] . '</td>';
		echo '<td>' . $arreglo_resultados[4] . '</td>';

    //boton para editar
		echo '<td> <button type="button" onclick="formProducto('.$arreglo_resultados[0].');">';
		echo 'Editar</button></td>';
    //boton para eliminar
		echo '<td> <button type="button" onclick="eliminarProducto('.$arreglo_resultados[0].');">';
		echo 'Eliminar</button></td>';
		echo '</tr>';
	}
?>
