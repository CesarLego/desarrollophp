<?php
//arrays
echo '<br>ARRAYS INDEXADOS<br>';

//definimos un arreglo en una variable carros
$carros = array("Volvo","BMW","Toyota");

var_dump($carros); //var_dump nos regresa el tipo de variable y los datos

// accedemos a cada elemento del arreglo mediante su indice numerico
echo "<br>" . $carros[0] . ", " . $carros[1] . " y " . $carros[2] . ".";

//-------------------------------------------------------------------------
//funcion count()
echo '<br><br>Funcion COUNT()<br>';

echo '<br> El array $carros tiene: '.count($carros). ' elementos'; //la salida será 3 

//---------------------------------------------------------------------------
//arrays asociativos
echo '<br><br>ARRAYS ASOCIATIVOS<br>';

//definimos un array asociativo en una variable de tipo array llamada $age
//peter, Ben, y Joe seran las clasves de nuestro arreglo (en lugar de indices)
$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");

//otra froma de definir un array asociativo
$age['Miguel'] = "12";
$age['Alex'] = "24";
$age['Daniela'] = "45";

//accedemos a cada elemento mediante su llave
echo "Peter tiene " . $age['Peter'] . " años";
echo "<br>Ben tiene " . $age['Ben'] . " años";
echo "<br>Joe tiene " . $age['Joe'] . " años";
echo "<br>Miguel tiene " . $age['Miguel'] . " años";
echo "<br>Alex tiene " . $age['Alex'] . " años";
echo "<br>Daniela tiene " . $age['Daniela'] . " años";

//-----------------------------------------------------------------------------
//Arrays Multidimensionales
echo '<br><br>ARRAYS MULTIDIMENSIONALES<br>';

//definimos nuestro array principal $carros2
$carros2 = array (
	//definimos un primer array interno, que contiene los valores, volvo,22,18
  array("Volvo",22,18),
  	//definimos un segundo array interno, que contiene los valores, bmw,15,13
  array("BMW",15,13),
  	//definimos un tercer array interno, que contiene los valores, saab,5,2
  array("Saab",5,2),
  	//definimos un cuarto array interno, que contiene los valores, land,17,15
  array("Land Rover",17,15)
);

//accdemos a cada elemento de los arreglos mediante sus indices

//aqui accedemos al elemento en la posicion 0 del array carros2, y despues a la posicion 0 del array interno, esto nos dara la marca
//para el stock accdemos a la posicion 1, del array interno
//para los vendidos, accdemos a la tercera posicion del array interno
echo $carros2[0][0].": stock: ".$carros2[0][1].", vendidos: ".$carros2[0][2].".<br>";
//aqui accedemos al segundo array interno del array carros2
echo $carros2[1][0].": stock: ".$carros2[1][1].", vendidos: ".$carros2[1][2].".<br>";
//aqui accedemos al tercer array interno del array carros2
echo $carros2[2][0].": stock: ".$carros2[2][1].", vendidos: ".$carros2[2][2].".<br>";
//aqui accedemos al cyuarto array interno del array carros2
echo $carros2[3][0].": stock: ".$carros2[3][1].", vendidos: ".$carros2[3][2].".<br>";

//-----------------------------------------------------------------------------
//FOREACH LOOPS
echo '<br><br>FOR EACH LOOPS<br>';

//definimos un array $cars con 3 valores string, volvo, bmw, y toyota
$cars = array("Volvo", "BMW", "Toyota");

//obtenemos el numero de elementos del array para neustra condicion en el ciclo for
$arrlength = count($cars); // la salida es 3
echo '<br>La longitud del arreglo $cars es: '.$arrlength;

//para i=o, mientras que i sea menor a 3 (longitud del array), incrementa i en 1 cada iteracion
for($i = 0; $i < $arrlength; $i++) {
	//imprimi el valor de $cars en la posición de i (la primera vuelta es cero, la segunda 1, la tercera 2, en esta tercera ronda i valdra 4, por lo que rompera la condicion y finalizara el ciclo)
  echo '<br>'.$cars[$i];
}

//-----------------------------------------------------------------------------
echo '<br><br>FOR EACH LOOP ARRAY ASOCIATIVOS<br>';

$lista_edades = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");

//Para cada elemento del array $lsita_edades, con el formato $clave,$valor
foreach($lista_edades as $clave => $valor) {
	//imprime el valor de la clave, y el valor del valor xD
  echo "Clave=" . $clave . ", Valor=" . $valor;
  echo "<br>";
}

//----------------------------------------------------------------------------
echo '<br><br>FOR EACH LOOP ARRAY MULTIDIMENSIONALES<br>';

//DEFINIMOS EL ARRAY3, QUE CONTENDRA 4 ARRAY INTERNOS, CADA UNO CON 3 ELEMENTOS
$carros3 = array (
  array("Volvo",22,18), //array interno en la posicion 0
  						//cada elemento del array sera referido como
  						//info_array_interno
  						//info_array_interno[0]=Volvo
						//info_array_interno[1]=22
						//info_array_interno[2]=18
  array("BMW",15,13), //array interno en la posicion 1
  array("Saab",5,2), //array interno en la posicion 2
  array("Land Rover",17,15) //array interno en la posicion 3
);
  
//CICLO FOR PARA RECORRER EL ARRAY

//ESTE PRIMER FOR RECORRERA SOLO LOS ARRAYS_INTERNO, LINEAS 103, 109,110,111  
for ($array_interno = 0; $array_interno < 4; $array_interno++) {

  //MOSTRARA EN PANTALLA CON NEGRITAS, ARRAY INTERNO, Y EL NUMERO DE ARRAY
  echo "<p><b>Array interno: $array_interno</b></p>";

  //solo es un formato html de lista desordenada (UnderderedList)
  echo "<ul>"; 

  //EL SEGUNDO FOR RECORRERA LOS ELEMENTOS DE CADA ARRAY INTERNO, QUE SON 3
  //UNO DE TIPO STRING, Y DOS DE TIPO INT
  for ($info_array_interno = 0; $info_array_interno < 3; $info_array_interno++) {
  	//<li> es list item, elemento de la lista, html que va dentro de <ul>
    echo "<li>".$carros3[$array_interno][$info_array_interno]."</li>";
  }
  echo "</ul>";
}

//UTILIZANDO LA LONGITUD VAIRABLE DE LOS ARRAY
echo '<br>Podemos obtener los limites de la iteracion con la funcion count()<br>';

$carros4 = array (
  array("Volvo",22,18), 
  array("BMW",15,13), 
  array("Saab",5,2), 
  array("Land Rover",17,15) 
);
  
//Cuando colocamos la condicion $array_interno < 4 del ejemplo anterior, lo hicmos con codigo duro, es decir, datos que sabemos que son correctos, pero que en un ambiente productivo, no sabremos, por lo que es mejor manejar la condicion del cilo for mediante la longitud del array, sin importar cual sea, con la funcion  count()
for ($array_interno = 0; $array_interno < count($carros4); $array_interno++) {
  echo "<p><b>Array interno: $array_interno</b></p>";
  echo "<ul>"; 
  for ($info_array_interno = 0; $info_array_interno < count($carros4[$array_interno]); $info_array_interno++) {
    echo "<li>".$carros4[$array_interno][$info_array_interno]."</li>";
  }
  echo "</ul>";
}


?>