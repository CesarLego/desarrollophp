<?php
//OPERADORES

//-----------------------------------------------------------------------------
//OPERADORES ARITMETICOS
echo '<br>OPERADORES ARITMETICOS<br>';

$x = 10;  
$y = 6;

echo '<br>X vale 10 , y vale 6';

//ADICION
echo '<br><br>Adicion<br>';
echo $x + $y;

//SUSTRACCION
echo '<br><br>Sustraccion<br>';
echo $x - $y;

//MULTIPLICACION
echo '<br><br>Multiplicacion<br>';
echo $x * $y;

//DIVISION
echo '<br><br>Division<br>';
echo $x / $y;

//MODULO
echo '<br><br>Modulo<br>';
echo $x % $y;

//EXPONENTES
echo '<br><br>Exponentes<br>';
echo $x ** $y;
echo'<br>';

//-----------------------------------------------------------------------------
//OPERADORES DE ASIGNACION
echo '<br>OPERADORES DE ASIGNACION<br>';

//ASIGNACION
echo '<br>Asignacion<br>';
$var1 = 10;
echo 'A var1 se le asigno con el signo = el valor de : '.$var1;

//ADICION
echo '<br><br>Adicion<br>';
$var2 = 20;
$var2 += 100;
echo 'A var2 se le asigno el valor de 20 y con += se le suma 100 : '.$var2;

//SUSTRACCION
echo '<br><br>Sustraccion<br>';
$var3 = 50;
$var3 -= 30;
echo 'A var3 se le asigno el valor de 50 y con -= se le resta 30 : '.$var3;

//MULTIPLICACION
echo '<br><br>Multiplicacion<br>';
$var4 = 10;
$var4_ = 6;
echo 'Multiplicacion de var4 igual a 10 por var4_ igual a 6 : '.$var4 * $var4_;

//DIVISION
echo '<br><br>Division<br>';
$var5 = 10;
$var5_ = 5;
echo 'Division de var5 igual a 10 entre var5_ igual a 5 : '.$var5 / $var5_;

//MODULO
echo '<br><br>Modulo<br>';
$var6 = 15;
$var6 %= 4;
echo 'A var6 se le asigno el valor de 15 y con %= se obtiene el modulo entre 4: '.$var6.' por que 15/4 es 3, 4x3 es 12, y el modulo es 3';

//-----------------------------------------------------------------------------
//OPERADORES DE Comparacion
echo '<br><br>OPERADORES DE COMPARASION<br>';

//EQUAL
echo '<br>Equal<br>';

$var7 = 100;
$var8 = "100";
$var9 = 200;

var_dump($var7 == $var8); // regresa TRUE por que los valores son iguales.

//IDENTICAL
echo '<br><br>Identical<br>';
var_dump($var7 == $var8); // regresa FALSE por que los valores no son el mismo tipo aunque los valores si lo sean.

//NOT EQUAL
echo '<br><br>Not equal<br>';
var_dump($var7 != $var8); // regresa falso por que los valores son iguales

//NOT EQUAL
echo '<br><br>Not equal <> <br>';
var_dump($var7 <> $var9); // regresa true por que los tipos son iguales

//NOT IDENTICAL
echo '<br><br>Not identical<br>';
var_dump($var7 !== $var8); // regresa true por que los tipos no son iguales

//GREATHER THAN
echo '<br><br>GREATER THAN<br>';
var_dump($var7 > $var9); //regresa false por que var9 es mayor

//LESS THAN
echo '<br><br>LESS THAN<br>';
var_dump($var7 < $var9); //regresa true por que var9 es mayor

//GREATHER EQUAL THAN
echo '<br><br>GREATER EQUAL THAN<br>';
var_dump($var7 >= $var9); //regresa false por que var9 es mayor

//LESS EQUAL THAN
echo '<br><br>LESS EQUAL THAN<br>';
var_dump($var7 <= $var9); //regresa true por que var9 es mayor

//SPACESHIP
echo '<br><br>SPACESHIP<br>';

$var10 = 5;  
$var11 = 10;

echo ($var10 <=> $var11); // regresa -1 porque $var10 es menor a var11
echo "<br>";

$var12 = 10;  
$var13 = 10;

echo ($var12 <=> $var13); // regresa 0 por que son iguales
echo "<br>";

$var14 = 15;  
$var15 = 10;

echo ($var14 <=> $var15); // regresa 1 por que var14 es mayor a var15

//----------------------------------------------------------------------------
//OPERADORES DE COMPARACION
echo '<br><br>OPERADORES DE INCREMENTO O DECREMENTO<br>';

//PREINCREMENTO
echo '<br><br>PRE INCREMENTO<br>';
echo ++$var15;

//POSTINCREMENTO
echo '<br><br>POST INCREMENTO<br>';
echo $var15++.'<br>';
echo $var15;

//PREDECREMENTO
echo '<br><br>PRE DECREMENTO<br>';
echo --$var15;

//POSTDECREMENTO
echo '<br><br>POST DECREMENTO<br>';
echo $var15--.'<br>';
echo $var15;

//----------------------------------------------------------------------------
//OPERADORES LOGICOS
echo '<br><br>OPERADORES LOGICOS<br>';

//AND   And  true si ambos son true
$var16 = 100;  
$var17 = 50;

if ($var16 == 100 and $var17 == 50) {
    echo "Hello world! AND";
}

//OR  Or  true si cualquiera de los dos es true
echo'<br>';
if ($var16 == 100 or $var17 == 80) {
    echo "Hello world! OR ";
}

//XOR Xor  true si cualquiera de los dos es true pero no ambos
echo'<br>';
if ($var16 == 100 xor $var17 == 80) {
    echo "Hello world! XOR";
}

//&& AND   true si ambos son true
echo'<br>';
if ($var16 == 100 && $var17 == 50) {
    echo "Hello world! &&";
}

//  || OR   true si cualquiera de los dos es true
echo'<br>';
if ($var16 == 100 || $var17 == 80) {
    echo "Hello world! ||";
}

// ! NOT    true si la variable no es true
echo'<br>';
if ($var16 !== 90) {
    echo "Hello world! !";
}

//------------------------------------------------------------------------
//OPERADORES STRING
echo '<br><br>OPERADORES STRING<br>';

//Concatenacion
echo '<br><br>CONCATENACION<br>';
$txt1 = "Hello";
$txt2 = " world!";
echo $txt1 . $txt2.'<br>';

//concatenacion con asignacion
echo '<br><br>CONCATENACION CON ASIGNACION<br>';
$txt1 = "Hello";
$txt2 = " world!";
$txt1 .= $txt2;
echo $txt1;

//------------------------------------------------------------------------


?>