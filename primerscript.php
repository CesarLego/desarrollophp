<?php //esta es la etiqueta con la que inicia cualquier script PHP

	//Función echo

echo "Hola Mundo desde la beca Virtual";
echo "<br>";
echo "Texto <b>combinado</b> con <p>html</p>";

	//print

print "<br>Hola mundo con print function<br>";
print "<b>Hola</b> mundo<br>";
	//Comentarios

echo "Ejecución del programa<br>";
//Línea de comentarios que no afectan la ejecutación
echo "Sigue la ejecución del programa<br>";
/*
Comentarios
en multiples lineas
que no afectan el programa
*/
echo "No se interrumpio el programa<br>";



?>
